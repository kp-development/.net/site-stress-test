﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Site.Stress.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            //if (args != null && args.GetUpperBound(0) >= 1)
            //{
                //string _url = args.GetValue(0).ToString();
                //int _times = Convert.ToInt32(args.GetValue(1));
                //Console.WriteLine("Stressing the site: " + _url + " " + _times + " Iterations");
                //Console.WriteLine("Have a little bit of patience, depending on the number of iterations, this could take a bit...");
                List<Sites> _sites = new List<Sites>();

                //_sites.Add(new Sites() { URL = "http://cc.getyou.onl", Iterations = 1000 });
                //_sites.Add(new Sites() { URL = "http://pc.getyou.onl", Iterations = 1000 });
                //_sites.Add(new Sites() { URL = "http://104.ls.getyou.onl", Iterations = 1000 });
                //_sites.Add(new Sites() { URL = "http://1047.ls.getyou.onl", Iterations = 1000 });
                //_sites.Add(new Sites() { URL = "http://ccb.ls.getyou.onl", Iterations = 1000 });
                //_sites.Add(new Sites() { URL = "http://wfcc.ls.getyou.onl", Iterations = 1000 });
                _sites.Add(new Sites() { URL = "http://test.o7t.in", Iterations = 10000000 });

                int _ct = _sites.Count;

                Parallel.For(0, _ct, j =>
                {
                    Console.WriteLine("Testing: " + _sites[j].URL);
                    Parallel.For(0, _sites[j].Iterations, i =>
                        {
                            Console.WriteLine("Iteration: " + i);
                            Task.Factory.StartNew(() => StressIt(_sites[j].URL));
                        });
                    //for (int i = 0; i < _sites[j].Iterations; ++i)
                    //{
                    //    Console.WriteLine("Iteration: " + i);
                    //    StressIt(_sites[j].URL);
                    //}
                });


                Console.WriteLine("Press \'q\' + Enter to quit.");
                while (Console.Read() != 'q') ;
            //}
            //else
            //{
            //    Console.WriteLine("Need to pass the URL and the number of iterations");
            //    Console.WriteLine("Press \'q\' + Enter to quit.");
            //    while (Console.Read() != 'q') ;
            //}
        }


        private static void StressIt(string _url)
        {
            string _ret = default(string);
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
                WebRequest _req = WebRequest.Create(_url + "?_=" + DateTime.Now.ToString());
                /*byte[] authBytes = Encoding.UTF8.GetBytes("root:Wr4ngl3r55".ToCharArray());
                _req.Headers["Authorization"] = "Basic " + Convert.ToBase64String(authBytes);*/
                ((HttpWebRequest)_req).UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US)";
                _req.Timeout = 100000;
                using (WebResponse _resp = _req.GetResponse())
                {
                    using (Stream _str = _resp.GetResponseStream())
                    {
                        using (StreamReader _sr = new StreamReader(_str, Encoding.UTF8))
                        {
                            _ret = _sr.ReadToEnd();
                            _sr.Close();
                        }
                        _str.Close();
                    }
                    _resp.Close();
                }
                _ret = default(string);
            }
            catch (WebException wex)
            {
            }
            catch (Exception ex)
            {
                _ret = default(string);
            }
        }

        private static bool AcceptAllCertifications(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

    }

    public class Sites
    {
        public string URL { get; set; }
        public int Iterations { get; set; }
    }

}
